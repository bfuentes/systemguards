<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'atomlab_child_enqueue_scripts' ) ) {
	function atomlab_child_enqueue_scripts() {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

		wp_enqueue_style( 'atomlab-style', ATOMLAB_THEME_URI . "/style{$min}.css" );
	}
}
add_action( 'wp_enqueue_scripts', 'atomlab_child_enqueue_scripts' );



/* Custom stylesheet with no dependencies, enqueued in the header */
function systemguards_custom_styles() {
	// Register my custom stylesheet
	wp_register_style('systemguards-style', get_stylesheet_directory_uri().'/style-systemguards.css', false, '1.0', 'all');
	// Load my custom stylesheet
	wp_enqueue_style('systemguards-style');
  }

add_action('wp_enqueue_scripts', 'systemguards_custom_styles',10001);



/* Custom script with no dependencies, enqueued in the header */
add_action('wp_enqueue_scripts', 'systemguards_custom_js', 10002);
function systemguards_custom_js() {
    wp_enqueue_script('systemguards-script', get_stylesheet_directory_uri().'/script-systemguards.js', false, '1.0', 'all');
}