#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Atomlab Child\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-02-03 05:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.0; wp-5.6\n"
"X-Domain: atomlab"

#. Name of the theme
msgid "Atomlab Child"
msgstr ""

#. URI of the theme
msgid "http://atomlab.thememove.com/"
msgstr ""

#. Author URI of the theme
msgid "http://thememove.com/"
msgstr ""

#. Author of the theme
msgid "ThemeMove"
msgstr ""
