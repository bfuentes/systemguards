<?php
/**
 * The template for displaying archive tags portfolio pages.
 *
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TM Atomlab
 * @since   1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_template_part( 'archive-portfolio' );
