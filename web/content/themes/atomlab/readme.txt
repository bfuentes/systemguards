=== TM Atomlab ===

Contributors: thememove
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 5.0.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Atomlab - Multi-Purpose Startup WordPress Theme

== Description ==

Atomlab brings the young and vibrant look to your website with high flexibility in customization. This is the theme of beautifully crafted pages and elements for multiple purposes.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.8.2 - August 13 2020 =
Fixed: Customize not working with WordPress 5.5 (Required update Insight Core)
Updated: Insight Core plugin - v.1.7.1
Updated: Revolution Slider plugin - v.6.2.18
Updated: WPBakery Page Builder - v.6.2.0
Updated: WPBakery Page Builder Clipboard - v.4.5.5
Updated: Out-date Woocommerce templates.
Added: new Instagram Feed plugin - v.3.8.7

= 1.8.1 - March 30 2020 =
Updated: Insight Core plugin - v.1.6.5
Updated: Revolution Slider plugin - v.6.2.2

= 1.8.0 - March 11 2020 =
Fixed: Counter shortcode not working properly.
Fixed: Fix minor style.
Updated: out-date Woocommerce templates.
Updated: Insight Core plugin - v.1.6.3
Updated: Revolution Slider plugin - v.6.2.1

= 1.7.0 - February 17 2020 =
1. Updated: Insight Core plugin, Improvement admin performance. - v.1.6.0
2. Updated: Revolution Slider plugin - v.6.1.8

= 1.6.0 - January 08 2020 =
1. Updated: WPBakery Page Builder plugin - v.6.1
2. Fixed: Pie Chart not showing number.
3. Fixed: product variable not change image when attribute changes.
4. Fixed: woo wishlist
5. Fixed: woo compare
6. Added: setting header type for archive portfolio/product.
7. Tweak: change default header type of single product.

= 1.5.3 - November 27 2019 =
1. Update plugin Insight Core v1.5.5.1 to compatible with WP 5.3.x
2. Update Revolution Slider plugin - v.6.1.5
3. Update out-date woocommerce templates

= 1.5.2 - September 24 2019 =
1. Update Revolution Slider plugin- v.6.1.2
2. Update WPBakery Page Builder plugin - v.6.0.5
3. WPBakery Page Builder (Visual Composer) Clipboard - v.4.5.4
4. Fix out-date woocommerce templates.

= 1.5.1 - July 26 2019 =
1. Update WPBakery Page Builder plugin - v.6.0.4
2. Update Revolution Slider plugin- v.6.0.6
3. Update Insight Core plugin- v.1.5.5.0

= 1.5.0 - July 01 2019 =
1. Update WPBakery Page Builder plugin - v.6.0.3
2. Fix shortcode to compatible with WPBakery 6.0.3
3. Improvement site performance.

= 1.4.9 - June 17 2019 =
1. Fix Header style 14 working not properly on Safari.

= 1.4.8 - April 22 2019 =
1. Update Insight Core plugin - v.1.5.4.9
2. Update out-date Woocommerce templates compatible with Woocommerce v.3.6.x
3. Fix smooth scroll in Chrome - v.73.xx

= 1.4.7 - March 26 2019 =
1. Update Revolution Slider plugin - v.5.4.8.3
2. WPBakery Page Builder (Visual Composer) Clipboard plugin - v.4.5.1

= 1.4.6 - Feb 27 2019 =
1. Fix footer parallax not working properly.
2. Update WPBakery Page Builder plugin - v.5.7
3. Update Revolution Slider plugin - v5.4.8.2
4. Update out-date woocommerce templates

= 1.4.5 - Jan 25 2019 =
1. Fix contact form 7 scripts enqueue.
2. Add option customize blog archive style.
3. Fix Product element filter not working.

= 1.4.4 - Jan 07 2019 =
1. Fix product variable working not properly.

= 1.4.3 - Dec 13 2018 =
1. Fix header sticky is not working properly.

= 1.4.2 - Dec 03 2018 =
1. Update out-date woocommerce templates
2. Update Insight Core plugin - v.1.5.4.5
3. Update WPBakery Page Builder plugin - v.5.6

= 1.4.1 - Nov 19 2018 =
1. Fix some images missing alt attribute.
2. Update Insight Core plugin - v1.5.4.4
3. Changed support chanel link.

= 1.4.0 - Nov 07 2018 =
1. Update Insight Core plugin - v1.5.4.2
2. Move Product Attribute Swatches to external plugin.
3. Improvement performance.

= 1.3.6 - Oct 31 2018 =
1. Update WPBakery Page Builder plugin - v.5.5.5
2. Update out-date template files. Compatible with Woocommerce latest version v3.5.0

= 1.3.5 - Oct 12 2018 =
1. Improvement Top Bar visibility.
2. Fix I-PAD auto wrap phone number with a tag, make text not inline.
3. Fix mobile menu not smooth scroll.

= 1.3.4 - Sep 25 2018 =
1. Fix social networks not showing custom icon font.
2. Fix body typography not working properly.

= 1.3.3 - Sep 19 2018 =
1. Fix single product variable slider not change when change any attribute.
2. Add hook/filter atomlab_register_meta_boxes to add Page Options meta box for custom post type.
3. Add Blank template for maintenance mode to custom maintenance page.

= 1.3.2 - Sep 11 2018 =
1. Update WPBakery Page Builder plugin - v.5.5.4

= 1.3.1 - Aug 17 2018 =
1. Add wrap link option for Box Icon shortcode.
2. Add nav option for Team Member Carousel Zigzag

= 1.3.0 - Aug 17 2018 =
1. Update swiper jquery.
2. Update out-date woo templates

= 1.2.9 - Jul 19 2018 =
1. Fix responsive issue.
2. Fix import wrong url
3. Fix php warning in Insight Core Update page.
4. Add custom visibility & spacing for breadcrumb in title bar.

= 1.2.8 - Jul 12 2018 =
1. Update WPBakery Page Builder (Visual Composer) Clipboard - v4.5.0
2. Customizable Visual Composer icon libraries.

= 1.2.7 - Jul 03 2018 =
1. Update WPBakery Page Builder - v5.5.2
2. Fix VC Row big triangle separator.
3. Fix php warning messages.

= 1.2.6 - Jun 22 2018 =
1. Fix coming soon title render not properly.
2. Update WPBakery Page Builder - v5.5.1

= 1.2.5 - Jun 22 2018 =
1. Update swiper slider library.
2. Fix slider frame shortcode.
3. Fix style in Coming Soon template.
4. Fix header not hide with hide option.
5. Update Revolution Slider plugin.
6. Update WPBakery Page Builder plugin.

= 1.2.4 - Jun 16 2018 =
1. Fix demo content importer.

= 1.2.3 - Jun 07 2018 =
1. Fix customize typography not change.

= 1.2.2 - Jun 04 2018 =
1. Fix minor style

= 1.2.1 - May 28 2018 =
1. Add option to link project page instead of single portfolio page.

= 1.2.0 - May 24 2018 =
1. Update Revolution Slider plugin.
2. Update Outdated woocommerce templates.

= 1.1.8 - May 17 2018 =
1. Fix php warning messages.

= 1.1.7 - May 16 2018 =
1. Improvement dashboard performance.
2. Update Insight Core plugin

= 1.1.6.1 - May 10 2018 =
1. Fixed single image popup not working.

= 1.1.6 - May 08 2018 =
1. Fixed shortcode pricing template.
2. Add attr alt for single image.
3. Fixed minor admin style
4. Customizable header type + title bar for any pages with add_filter.

= 1.1.5 - May 02 2018 =
1. Fixed one page menu JS error.
2. Update Revolution Slider plugin.

= 1.1.4 - April 23 2018 =
1. Update Insight Core plugin
2. Custom animate duration for svg icons.

= 1.1.3 - April 17 2018 =
1. Fix some svg files not load.
2. Update Insight Core plugin.
3. Fix Social Networks sharing.

= 1.1.2 - April 14 2018 =
1. Disabled embed video fullscreen.
2. Fix Visual Composer Frontend builder.
3. Fix warning message of tag could widget.

= 1.1.1 - April 13 2018 =
1. Fix Accordion shortcode.
2. Fix import demo content.
3. Add header button to mobile menu.
4. Improvement header template code.

= 1.1 - April 11 2018 =
1. Fix Sub-menu typography.
2. Fix shortcode Instagram.
3. Fix shortcode Blog + Portfolio infinity load.
4. Added 2 header styles
5. Added 1 top bar style
6. Added 3 new homepages

= 1.0.6 - April 06 2018 =
1. Fix Visual Composer Frontend builder.
2. Integrated download child theme in dashboard.
3. Better select for typography field type in Customize.
4. Change shortcode Twitter use date format from admin setting.
5. Shortcode Accordion can custom count from number.

= 1.0.5 - April 02 2018 =
1. Fix auto update theme not working.
2. Fix can't import demo content with host setting: max_execution_time: 0

= 1.0.4 - March 30 2018 =
1. Fix shortcode Button popup video action not working properly
2. Fix linea fonts not render properly

= 1.0.3 - March 29 2018 =
1. Fix Error Warning
2. Fix Responsive Style
3. Update Revolution Slider Plugin
4. Add settings to custom for cookie notice messages.

= 1.0.2 - March 27 2018 =
1. Update Import Content
2. Fix minor style
3. Improve the performance.

= 1.0.1 - March 27 2018 =
1. Update Import Content
2. Fix minor style

= 1.0.0 - March 27 2018 =
* Initial Release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
